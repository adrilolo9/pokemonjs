const { Console } = require('console');
const express = require('express');
const sql = require('mssql')
var fs = require('fs');

const app = express();

const server = app.listen(3001, function() {
    console.log('server running on port 3001');
});

const io = require('socket.io')(server, { cors: {
    origin: '*',
  }});

const config = {
    user: 'pokeadmin',
    password: 'Patata123',
    server: '192.168.43.121',
    database: 'PokemonJS',
    port: 1433
}

sql.connect(config, function(err) {
    if (err) console.log(err);

    let sqlRequest = new sql.Request();

    let sqlQuery = 'Select * from Results';

    sqlRequest.query(sqlQuery, function (err, data) {
        if (err) console.log(err);

        console.log(data.recordset);

        let newResults = [];
        for(let key in data){
        if(key === "recordsets"){
            data[key].forEach(arr =>{
                arr.forEach(obj =>{
                    newResults.push(obj);
                });
            })
        }
        }

        console.log(newResults);

        fs.writeFileSync('sqloutput.json', JSON.stringify(newResults));

        sql.close();
    });
});

io.on('connection', function(socket) {

    var data;
    var firstJson = fs.readFileSync('clients.json');
    if (firstJson != undefined || firstJson.length != 0) {
        var firstJsonObject = JSON.parse(firstJson);
        data = firstJsonObject;
        io.emit('CONNECTION', data);
    }

    socket.on('LOGIN', function(username) {
        var json = fs.readFileSync('clients.json');
        if (json == undefined || json.length == 0) {
            data = [];
            data.push([username, 'available']);
            fs.writeFileSync('clients.json', JSON.stringify(data));
        }
        else {
            var jsonObject = JSON.parse(json);
            jsonObject.push([username, 'available']);
            data = jsonObject;
            fs.writeFileSync('clients.json', JSON.stringify(data));
        }
        io.emit('CONNECTION', data);
    });

    socket.on('RESET', function() {
        fs.writeFileSync('clients.json', JSON.stringify([]));
        data = [];
        io.emit('RESETED', data);
    });

    socket.on('DUEL', function(users) {
        io.emit('INCOMINGDUEL', users);
    });

    socket.on('DISCONNECT', function(user) {
        var json = fs.readFileSync('clients.json');
        if (json != undefined || json.length != 0) {
            var jsonObject = JSON.parse(json);
            var id = 0;
            var trobat = false;
            while (!trobat){
                if (jsonObject[id][0] == user) {
                    trobat = true;
                    jsonObject.splice(id,1);
                }
                else{
                    id++;
                }
            }        
            data = jsonObject;
            fs.writeFileSync('clients.json', JSON.stringify(data));
            io.emit('CONNECTION', data);
        }
    });

    socket.on('CREATEBATTLE', function(users) {
        io.emit('BATTLE', users);
    });

    socket.on('CHANGEPOKEMON', function(data) {
        console.log("change pokemon");
        io.emit('POKEMONCHANGE', data);
    });

    socket.on('CHANGETURN', function(data) {
        io.emit('TURN', data);
    });

    socket.on('SENDATTACK', function(data) {
        console.log("server attack");
        io.emit('ATTACK', data);
    });

    socket.on('FINISHBATTLE', function(data) {
        

        try{
            sql.connect(config, function(err) {
                if (err) console.log(err);
            
                let sqlRequest = new sql.Request();
            
                let sqlQuery = "insert into Results (Player1, Player2, Result) values ('" + data[0] + "', '" + data[1] + "', '" + data[2] + "')";
            
                sqlRequest.query(sqlQuery, function (err, data) {
                    if (err) console.log(err);
                    sql.close();
                });
            });

            sql.connect(config, function(err) {
                if (err) console.log(err);
            
                let sqlRequest = new sql.Request();
            
                let sqlQuery = 'Select * from Results';
            
                sqlRequest.query(sqlQuery, function (err, data) {
                    if (err) console.log(err);
            
                    console.log(data.recordset);
            
                    let newResults = [];
                    for(let key in data){
                    if(key === "recordsets"){
                        data[key].forEach(arr =>{
                            arr.forEach(obj =>{
                                newResults.push(obj);
                            });
                        })
                    }
                    }
            
                    console.log(newResults);
            
                    fs.writeFileSync('sqloutput.json', JSON.stringify(newResults));
            
                    sql.close();
                });
            });
        }
        catch{}

        io.emit('FINISH', data);
    });
});