import Vue from 'vue';
import App from './App.vue';
import store from './store';
import vuetify from './plugins/vuetify';
import moment from 'moment';
import { MdButton, MdContent, MdTabs } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import router from './router'

Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdTabs)


Vue.config.productionTip = false;
Vue.prototype.moment = moment;

new Vue({
  store,
  vuetify,
  router,
  render: (h) => h(App)
}).$mount('#app');